sap.ui.define([], function () {
	"use strict";

	return {
		
		translateText: function(value){
			if (value){
				return this.getTranslation(value);
			}
        }
        
	};
});