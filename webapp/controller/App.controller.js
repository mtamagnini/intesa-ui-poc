sap.ui.define([
  "it/presales/cfapps/web/intesa-poc/controller/BaseController"
], function(Controller) {
  "use strict";

  return Controller.extend("it.presales.cfapps.web.intesa-poc.controller.App", {});
});
