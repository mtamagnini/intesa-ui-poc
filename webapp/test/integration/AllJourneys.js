sap.ui.define([
  "sap/ui/test/Opa5",
  "it/presales/cfapps/web/intesa-poc/test/integration/arrangements/Startup",
  "it/presales/cfapps/web/intesa-poc/test/integration/BasicJourney"
], function(Opa5, Startup) {
  "use strict";

  Opa5.extendConfig({
    arrangements: new Startup(),
    pollingInterval: 1
  });

});
