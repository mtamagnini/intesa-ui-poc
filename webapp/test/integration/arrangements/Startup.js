sap.ui.define([
  "sap/ui/test/Opa5"
], function(Opa5) {
  "use strict";

  return Opa5.extend("it.presales.cfapps.web.intesa-poc.test.integration.arrangements.Startup", {

    iStartMyApp: function () {
      this.iStartMyUIComponent({
        componentConfig: {
          name: "it.presales.cfapps.web.intesa-poc",
          async: true,
          manifest: true
        }
      });
    }

  });
});
